package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	ArrayList<FridgeItem> fridge = new ArrayList<>();

	@Override
	public int nItemsInFridge() {
		int numbItems = 0;
		
		for(int i = 0; i < fridge.size(); i++) {
			
			numbItems +=1;
				
		}	
		return numbItems;
	}
	
	@Override
	public int totalSize() {
		int size = 20;
		return size;
	}




	@Override
	public boolean placeIn(FridgeItem item) {
		if (fridge.size() <  20) {
			fridge.add(item);
			return true;
		}
			
		return false;
	}
	
	@Override
	public void takeOut(FridgeItem item) {
		if(fridge.contains(item)) {
			fridge.remove(item);
		}
		else {
			 throw new NoSuchElementException();
		}
	}

	@Override
	public void emptyFridge() {
		fridge.clear();
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredFridge = new ArrayList<>();
		for(FridgeItem food : fridge) {
			if(food.hasExpired()) {
				expiredFridge.add(food);
			}
		}
		for(FridgeItem food : expiredFridge) {
			fridge.remove(food);
		}
		
		
		return expiredFridge;
	}

}
